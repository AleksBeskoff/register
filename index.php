<?php

/**
 * основная страница - основной контроллер
 * 
 * @author aleks
 */
session_start();

if (isset($_SESSION['lang'])) {
    $langSession = $_SESSION['lang'];
} else {
    $langSession = '';
}

if (isset($_GET['param'])) {
    $param = htmlspecialchars($_GET['param']);
} else {
    $param = '';
}

//организуем выбор языка из имеющихся языковых файлов
if ($langSession != '') {
    include_once 'lang/' . $langSession . '.php';
} else {
    include_once 'lang/en.php';
}

//подключаем файлы с классами
include_once 'modelCls.php';
include_once 'ctrlCls.php';

//инициализируем объекты подключенных классов
$model = new modelCls();
$ctrl = new ctrlCls($model);

include_once 'views/head.php';

//подгружаем необходимый вид в зависимости от проведенных действий
switch ($param) {
    case 'register':
        include_once 'views/viewInput.php';
        break;
    case 'regformaction':
        include_once 'views/viewUpload.php';
        break;
    case 'userinfo':
        include_once 'views/viewUser.php';
        break;
    default:
        include_once 'views/viewLogin.php';
}

include_once 'views/footer.php';
