<?php
/**
 * Массив для английской локализации
 *
 * @author aleks
 */

$lang = array(
    'fname'=>'Your name',
    'lname'=>'Surname',
    'register'=>'Check in',
    'email'=>'Your email',
    'pass'=>'Your password',
    'conf_pass'=>'Confirm the password',
    'dbith'=>'Date of Birth',
    'country'=>'Country of Residence',
    'Russia'=>'Russian Federation',
    'Belarus'=>'Republic of Belarus',
    'Ukrain'=>'Ukraine',
    'textaria'=>'Additional information (optional)',
    'sign_up'=>'Sign up!',
    'its_free'=>'It is absolutely free and safe!',
    'sign_in'=>'Sign in',
    'choice_file'=>'Select the file with the following parameters: ',
    'login_pass'=>'Enter your e-mail and password!',
    'user_information'=>'User information:',
    'no_email_in_base'=>'E-mail address is not in the database!',
    'no_pass_in_base'=>'Wrong password!',
    'image_not_checked'=>'Image not been tested!',
    'error_download_image_file'=>'Error loading image!',
    'error_pass'=>'Password least 3 characters',
    'error_mail'=>'Invalid e-mail address',
    'reg_success'=>'Registration was successful, log in!',
    'reg_again'=>'Try it again!',
    'register_result'=>'Result User registration',
    'duplicate_email'=>'The email address is already in use!'
);
