<?php

/**
 * Устанавливаем переменную сессии для выбора локализации
 *
 * @author aleks
 */
session_start();
$lang = htmlspecialchars($_GET['lang']);
$_SESSION['lang']=$lang;
return true;

