<?php
if (!isset($langSession)) {
    exit; //если обращаются отдельно к конкретной странице - выходим    
}
$arrPost = array('userEmail' => 'usr_default_mail', 'passUser' => 0);

if (isset($_POST['useremail'])) {
    $arrPost['userEmail'] = htmlspecialchars($_POST['useremail']);
}
if (isset($_POST['passuser'])) {
    $arrPost['passUser'] = htmlspecialchars($_POST['passuser']);
}
?>
<div class="container">
    <div class="row">        
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <h2><?php echo $lang['user_information']; ?></h2>
            <hr class="colorgraph">
            <?php
            if ($ctrl->fnGetUserInfo($arrPost)) {                
                ?>
                <table width="100%" class="table table-striped">
                    <tr>
                        <td colspan="2"><img src="<?php echo $ctrl->publicData['fotoUser']; ?>" alt="alt_foto_user" width="300" height="333" /></td>                    
                    </tr>
                    <tr class = "active">
                        <td  width = "35%" class="typeInfo"><?php echo $lang['fname']; ?>:</td>
                        <td  width = "65%" class = "userInfo"><?php echo $ctrl->publicData['fnameUser']; ?></td>
                    </tr>
                    <tr class = "active">
                        <td  width = "35%" class="typeInfo"><?php echo $lang['lname']; ?>:</td>
                        <td  width = "65%" class = "userInfo"><?php echo $ctrl->publicData['lnameUser']; ?></td>
                    </tr>
                    <tr class = "active">
                        <td  width = "35%" class="typeInfo"><?php echo $lang['email']; ?>:</td>
                        <td  width = "65%" class = "userInfo"><?php echo $ctrl->publicData['emailUser']; ?></td>
                    </tr>
                    <tr class = "active">
                        <td  width = "35%" class="typeInfo"><?php echo $lang['dbith']; ?>:</td>
                        <td  width = "65%" class = "userInfo"><?php echo $ctrl->publicData['dbUser']; ?></td>
                    </tr>
                    <tr class = "active">
                        <td  width = "35%" class="typeInfo"><?php echo $lang['country']; ?>:</td>
                        <td  width = "65%" class = "userInfo"><?php echo $lang[$ctrl->publicData['nameCountry']]; ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><p><?php echo $lang['textaria']; ?>:</p><div><?php echo $ctrl->publicData['descUser']; ?></div></td>                    
                    </tr>                    
                </table>
                <?php
            } else {
                echo '<div class="reg_error">'.$lang[$ctrl->errorMessage].'</div>';
            }
            ?>
            <hr class="colorgraph">
            <div class="row">
                <div class="col-xs-6 col-md-6"><a href="/" class="btn btn-success btn-block btn-lg"><?php echo $lang['sign_in'] ?></a></div>
                <div class="col-xs-6 col-md-6"><a href="/index.php?param=register" class="btn btn-success btn-block btn-lg"><?php echo $lang['register'] ?></a></div>            
            </div>
        </div>        
    </div>   
</div>