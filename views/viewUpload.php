<?php
if (!isset($langSession)) {
    exit; //если обращаются отдельно к конкретной странице - выходим
}

//возвращаем ссылку перевода на предыдущую страницу
function fnPrevPage($lang,$message){
    return '<p><a href="javascript:history.back();">'.$lang[$message].'</a></p>';
}

//возвращаем стилизованное сообщение об ошибке в результате проведенных операций
function fnRetError($ctrl,$lang,$message='reg_again'){
    return '<div class="reg_error"><p>'.$lang[$ctrl->errorMessage].'</p>'.fnPrevPage($lang,$message).'</div>';
}

//возвращаем результат регистрации пользователя
function fnControlAndAdd($ctrl,$lang) {
    if (isset($_FILES['userfile'])) {
        $file = $_FILES['userfile'];
        if ($ctrl->fnCheckImageFile($file)) {
            if ($ctrl->fnDowloadImageFile($file)) {                
                if ($ctrl->fnAddUserBase($_POST, $ctrl->imageFilePath)) {
                    return '<div class="reg_success">'.$lang['reg_success'].'</div>';
                } else {
                    return fnRetError($ctrl,$lang,'reg_again');
                }
            } else {
                return fnRetError($ctrl,$lang,'reg_again');
            }
        } else {
            return fnRetError($ctrl,$lang,'reg_again');
        }
    } else {
        return '<div class="reg_error"><p>error_choice_image_file</p>'.fnPrevPage($lang,'reg_again').'</div>';
    }
}
?>
<div class="container">
    <div class="row">        
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <h2><?php echo $lang['register_result']; ?></h2>
            <hr class="colorgraph">
            <?php echo fnControlAndAdd($ctrl,$lang); ?>
            <hr class="colorgraph">
            <div class="row">
                <div class="col-xs-6 col-md-6"><a href="/" class="btn btn-success btn-block btn-lg"><?php echo $lang['sign_in']; ?></a></div>
                <div class="col-xs-6 col-md-6"><a href="/index.php?param=register" class="btn btn-success btn-block btn-lg"><?php echo $lang['register'] ?></a></div>            
            </div>
        </div>        
    </div>   
</div>
