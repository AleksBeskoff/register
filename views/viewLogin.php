<?php
if (!isset($langSession)) {
    exit; //если обращаются отдельно к конкретной странице - выходим
}
?>

<div class="container">

    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <form role="form" method="POST" action="index.php?param=userinfo">
                <h2><?php echo $lang['login_pass']; ?></h2>
                <hr class="colorgraph">
                <div class="input-group">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
                    <input name="useremail" type="email" class="form-control input-lg" placeholder="<?php echo $lang['email']; ?>" required tabindex="1" />
                </div>
                <br>
                <div class="input-group">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                    <input name="passuser" type="password" class="form-control input-lg" placeholder="<?php echo $lang['pass']; ?>" required tabindex="2" />
                </div>                  

                <hr class="colorgraph">
                <div class="row">
                    <div class="col-xs-6 col-md-6"><input type="submit" value="<?php echo $lang['sign_in']; ?>" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
                    <div class="col-xs-6 col-md-6"><a href="/index.php?param=register" class="btn btn-success btn-block btn-lg"><?php echo $lang['register'] ?></a></div>
                </div>
            </form>
        </div>
    </div>   
</div>