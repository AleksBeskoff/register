<?php
if (!isset($langSession)) {
    exit; //если обращаются отдельно к конкретной странице - выходим
}
?>

<html>
    <head>
        <meta charset="UTF-8" />
        <title>Registered</title>
        <!-- JS -->
        <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
        <script type="text/javascript" src="js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
        <script type="text/javascript" src="js/projectScript.js"></script>
        <!-- CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css" />
        <link href="css/style.css" rel="stylesheet" />
    </head>
    <body>
        <p class="col-md-8 btn-group">
            <?php switch ($langSession) {
                case 'en':
                    ?>
                    <button id="langEn" type="button" class="btn btn-primary btn-sm">English</button>
                    <button id="langRu" type="button" class="btn btn-default btn-sm">Русский</button>
                    <?php
                    break;
                case 'ru':
                    ?>
                    <button id="langEn" type="button" class="btn btn-default btn-sm">English</button>
                    <button id="langRu" type="button" class="btn btn-primary btn-sm">Русский</button>
                    <?php
                    break;
                default :
                    ?>
                    <button id="langEn" type="button" class="btn btn-primary btn-sm">English</button>
                    <button id="langRu" type="button" class="btn btn-default btn-sm">Русский</button>
    <?php
}
?>                
        </p>
