<?php
if (!isset($langSession)) {
    exit; //если обращаются отдельно к конкретной странице - выходим
}
?>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <form role="form" method="POST" action="/index.php?param=regformaction" enctype="multipart/form-data">
                <h2><?php echo $lang['sign_up']; ?>&nbsp;<small><?php echo $lang['its_free']; ?></small></h2>
                <hr class="colorgraph">
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="form-group">
                            <input type="text" name="first_name" class="form-control input-lg" placeholder="<?php echo $lang['fname']; ?>" tabindex="1" required maxlength="70"/>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="form-group">
                            <input type="text" name="last_name" class="form-control input-lg" placeholder="<?php echo $lang['lname']; ?>" required tabindex="2" maxlength="70" />
                        </div>
                    </div>
                </div>                
                <div class="form-group">
                    <input type="email" name="email" id="email" class="form-control input-lg" placeholder="<?php echo $lang['email']; ?>" tabindex="4" required maxlength="100">
                </div>
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="form-group">
                            <input type="password" name="password" class="form-control input-lg" placeholder="<?php echo $lang['pass']; ?>" tabindex="5" required maxlength="20">
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="form-group">
                            <input type="password" name="password_confirmation" class="form-control input-lg" placeholder="<?php echo $lang['conf_pass']; ?>" tabindex="6" required maxlength="20">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="form-group">
                            <input type="text" name="dbith" id="dbith" class="form-control input-lg" tabindex="7" readonly placeholder="<?php echo $lang['dbith']; ?>">                            
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <div class="form-group">
                            <select name="country" class="form-control input-lg" tabindex="8">
                                <?php
                                if ($ctrl->fnSelectCtrl()) {
                                    foreach ($ctrl->publicData as $row) {
                                        ?>
                                        <option value="<?php echo $row['idCountry']; ?>"><?php echo $lang[$row['nameCountry']]; ?></option>                  
                                        <?php
                                    }
                                }
                                ?>                                
                            </select>    
                        </div>
                    </div>
                </div>
                <div class="form-group">                    
                    <textarea class="form-control noresize input-lg" name="desc" placeholder="<?php echo $lang['textaria']; ?>"></textarea>                    
                </div>                
                <div class="form-group">                    
                    <?php echo $lang['choice_file']; ?>max - 1000 kb, ext - *.jpeg, *.png, *.gif.
                    <input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
                    <input type="file" name="userfile" class="form-control input-lg" accept="image/jpeg,image/png,image/gif" required />
                </div>    
                <div class="form-group">                    

                    <hr class="colorgraph">
                    <div class="row">
                        <div class="col-xs-6 col-md-6"><input type="submit" id="submit" value="<?php echo $lang['register'] ?>" class="btn btn-primary btn-block btn-lg" tabindex="7"></div>
                        <div class="col-xs-6 col-md-6"><a href="/" class="btn btn-success btn-block btn-lg"><?php echo $lang['sign_in']; ?></a></div>
                    </div>
            </form>
        </div>
    </div>