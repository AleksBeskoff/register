-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Фев 06 2017 г., 20:14
-- Версия сервера: 5.5.54-0ubuntu0.14.04.1
-- Версия PHP: 5.5.9-1ubuntu4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `temp_base`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tabCountry`
--

CREATE TABLE IF NOT EXISTS `tabCountry` (
  `idCountry` int(11) NOT NULL AUTO_INCREMENT,
  `nameCountry` varchar(100) NOT NULL,
  PRIMARY KEY (`idCountry`),
  UNIQUE KEY `nameCountry` (`nameCountry`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `tabCountry`
--

INSERT INTO `tabCountry` (`idCountry`, `nameCountry`) VALUES
(2, 'Belarus'),
(1, 'Russia'),
(3, 'Ukrain');

-- --------------------------------------------------------

--
-- Структура таблицы `tabUsers`
--

CREATE TABLE IF NOT EXISTS `tabUsers` (
  `idUser` int(11) NOT NULL AUTO_INCREMENT,
  `fnameUser` varchar(70) NOT NULL,
  `lnameUser` varchar(70) NOT NULL,
  `emailUser` varchar(100) NOT NULL,
  `passUser` varchar(50) NOT NULL,
  `dbUser` date NOT NULL,
  `countryUser` tinyint(4) NOT NULL,
  `fotoUser` varchar(100) NOT NULL,
  `descUser` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`idUser`),
  UNIQUE KEY `emailUser` (`emailUser`),
  KEY `passUser` (`passUser`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Дамп данных таблицы `tabUsers`
--

INSERT INTO `tabUsers` (`idUser`, `fnameUser`, `lnameUser`, `emailUser`, `passUser`, `dbUser`, `countryUser`, `fotoUser`, `descUser`) VALUES
(23, 'Vin', 'Deizel', 'fname@mail.ru', 'fcea920f7412b5da7be0cf42b8c93759', '2010-01-04', 2, 'images/8613Diesel-Caricature.jpg', 'actor superman actor superman actor superman actor superman actor superman actor superman actor superman actor superman ');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
