<?php

/**
 * Класс, реализующий функции вспомогательного контроллера
 *
 * @author aleks
 */
class ctrlCls {

    private $model; //модель
    private $arrPost;   //массив, содержащий введенные данные формы
    public $errorMessage;   //сообщения об ошибки
    public $imageFilePath;  //папка хранения графичских файлов
    public $publicData; //массив, содержащий полученные данные в ходе обращения к базе данных

    //конструктор класса, инициализируем класс модели

    public function ctrlCls($model) {
        $this->model = $model;
        return true;
    }

    /**
     * выборка данных с использованием ф-ии модель-класса
     * @return boolean
     */
    public function fnSelectCtrl() {
        if ($this->model->fnSelectModel()) {
            $this->publicData = $this->model->publicData;
            return true;
        } else {
            return false;
        }
    }

    /**
     * проверка выбранного граффического файла
     * @return boolean
     */
    public function fnCheckImageFile($file) {
        if ($file['name'] == '' or $file['size'] == 0) {
            $this->errorMessage = "image_not_checked";
            return false;
        }
        $getMime = explode('.', $file['name']);
        $mime = strtolower(end($getMime));
        $types = array('jpg', 'png', 'gif', 'jpeg');
        // если расширение не входит в список допустимых - return
        if (!in_array($mime, $types)) {
            $this->errorMessage = "image_not_checked";
            return false;
        }
        return true;
    }

    /**
     * загрузка файла на сервер в указанную дирректорию
     * @return boolean
     */
    public function fnDowloadImageFile($file) {
        $updir = "images/";
        // формируем уникальное имя картинки: случайное число и name
        $name = mt_rand(0, 10000) . $file['name'];
        if (move_uploaded_file($file['tmp_name'], $updir . $name)) {
            $this->imageFilePath = $updir . $name;
            return true;
        } else {
            $this->errorMessage = "error_download_image_file";
            return false;
        }
    }

    /**
     * добавляем данные о пользователе в базу данных
     * @return boolean
     */
    function fnAddUserBase($arrPost, $imagePath) {
        if ($this->fnCheckPost($arrPost, $imagePath)) {
            $this->model->fnInsUser($this->arrPost);
            return true;
        } else {
            //$this->errorMessage = "error_check_user_data";
            return false;
        }
    }

    /**
     * обработка введенных данных на стороне сервера и вызов проверки
     * @return boolean
     */
    private function fnCheckPost($arrPost, $imagePath) {
        $arr['fnameUser'] = htmlspecialchars($arrPost['first_name']);
        $arr['lnameUser'] = htmlspecialchars($arrPost['last_name']);
        $arr['emailUser'] = htmlspecialchars($arrPost['email']);
        $arr['passUser1'] = htmlspecialchars($arrPost['password']);
        $arr['passUser2'] = htmlspecialchars($arrPost['password_confirmation']);
        $arr['dbUser'] = htmlspecialchars($arrPost['dbith']);
        $arr['countryUser'] = htmlspecialchars($arrPost['country']);
        $arr['fotoUser'] = $imagePath;
        $arr['descUser'] = htmlspecialchars($arrPost['desc']);

        if (!$this->fnCheckFields($arr['passUser1'], $arr['passUser2'], $arr['emailUser'], false)) {
            return false;
        }
        $this->arrPost = $arr;
        return true;
    }

    /**
     * проверка введенных данных на стороне сервера
     * @return boolean
     */
    private function fnCheckFields($passUser1, $passUser2, $emailUser, $flag = true) {
        if ($flag == false) {
            if (!$this->model->fnCheckMail($emailUser)) {
                $this->errorMessage = 'duplicate_email';
                return false;
            }
        }
        if (strlen($passUser1) < 3 or $passUser1 != $passUser2) {
            $this->errorMessage = "error_pass"; //длинна пароля должна быть более 3 символов 
            return false;
        }
        if (!filter_var($emailUser, FILTER_VALIDATE_EMAIL)) {
            $this->errorMessage = "error_mail"; //некорректный, либо не введен email 
            return false;
        }
        return true;
    }

    /**
     * обработка авторизации пользователя на стороне сервера
     * @return boolean
     */
    public function fnGetUserInfo($arrPost) {
        if (!$this->fnCheckFields($arrPost['passUser'], $arrPost['passUser'], $arrPost['userEmail'])) {
            return false;
        }
        if (!$this->model->fnSelectModel($arrPost)) {
            $this->errorMessage = $this->model->modelErrorMessage;
            return false;
        }
        $this->publicData = $this->model->publicData[0];
        return true;
    }

}
