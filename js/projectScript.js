//устанавливаем нужное нам значение языкового параметра php-сессии через ajax-запрос
function setSession(lang) {
    $.ajax({
        url: 'lang/ajaxLang.php',
        data: 'lang=' + lang,
        success: function () {
            location.reload();
        }
    });
}

$(document).ready(function () {
    var pattern = /^[a-z0-9_-]+@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/i;
    var email = $('#email');

    //блокируем кнопку формы пока не пройдет контроль    
    $('#submit').attr('disabled', true);

    //установка языковой переменной сессии с использованием аякс
    $("#langEn").on('click', function () {
        setSession('en');
    });
    $("#langRu").on('click', function () {
        setSession('ru');
    });

    //инициализируем календарь для заданного текстового поля
    $(function () {
        $('#dbith').datetimepicker({viewMode: 'years', pickTime: false});
    });

    //проверка валидности заполнения поля "электронной почты"
    email.blur(function () {
        if (email.val().search(pattern) === 0) {
            $('#submit').attr('disabled', false);
            email.removeClass().addClass('form-control input-lg ok');
        } else {
            email.tooltip();
            email.removeClass().addClass('form-control input-lg error');
            $('#submit').attr('disabled', true);
        }
    });
});