<?php

/**
 * класс, реализующий модель проекта, исполняет запросы от класса-контроллера 
 *
 * @author aleks
 */
class modelCls {

    private $conn;      //коннект к базе данных
    public $modelErrorMessage;  //сообщения об ошибке
    public $publicData; //массив с данными, полученными в результате исполнения запроса к базе данных

    //конструктор

    function modelCls() {
        $username = 'temp_base';
        $upassword = '261971';
        $options = array(PDO:: MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');
        try {
            $this->conn = new PDO('mysql:host=localhost;dbname=temp_base', $username, $upassword, $options);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
            exit();
        }
    }

    /**
     * выполнение запроса выборки данных из базы данных по запросу
     * @return array
     */
    private function fnSetSelect($sql, $arrCriteria = array()) {
        try {
            $query = $this->conn->prepare($sql);
            $query->execute($arrCriteria);
            return $query->fetchAll();
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
            exit();
        }
    }

    /**
     * выборка данных из базы данных по запросу
     * @return boolean
     */
    public function fnSelectModel($arrPost = array()) {
        if (count($arrPost) == 0) {
            $sql = "SELECT idCountry, nameCountry FROM tabCountry ORDER BY idCountry";
            $this->publicData = $this->fnSetSelect($sql);
        } else {
            $sql = "SELECT `idUser` FROM `tabUsers` WHERE `emailUser` = :email";
            if (count($this->fnSetSelect($sql, array('email' => $arrPost['userEmail']))) == 0) {
                $this->modelErrorMessage = 'no_email_in_base'; //введенный адрес электронной почты отсуствтует в базе данных
                return false;
            }
            $sql1 = "SELECT `tabUsers`.`fnameUser`,`tabUsers`.`lnameUser`,`tabUsers`.`emailUser`,`tabUsers`.`dbUser`,`tabUsers`.`fotoUser`,`tabUsers`.`descUser`,`tabCountry`.`nameCountry` ";
            $sql1 = $sql1 . "FROM `tabUsers`INNER JOIN`tabCountry` ON `tabUsers`.`countryUser`=`tabCountry`.`idCountry` ";
            $sql1 = $sql1 . "WHERE `tabUsers`.`emailUser`= :email AND `tabUsers`.`passUser`= :pass";
            $arrParam = array('email' => $arrPost['userEmail'], 'pass' => md5($arrPost['passUser']));
            $this->publicData = $this->fnSetSelect($sql1, $arrParam);
            if (count($this->publicData) == 0) {
                $this->modelErrorMessage = 'no_pass_in_base'; //Вы ввели некорректный пароль
                return false;
            }
        }
        return true;
    }

    /**
     * поиск электронной почты в базе данных
     * @return boolean      
     */
    public function fnCheckMail($userMail) {
        $sql = "SELECT `idUser` FROM `tabUsers` WHERE `emailUser` = :email";
        if (count($this->fnSetSelect($sql, array('email' => $userMail))) > 0) {
            return false;
        }
        return true;
    }

    /**
     * преобразуем полученную дату в формат, приемлемый для mysql
     * @return string 
     */
    private function fnFormatDate($dbUser) {
        if (!preg_match('#^\d{2}\/\d{2}\/\d{4}$#', $dbUser)) {
            $dbUser = "01/01/2000";
        }
        $db = explode("/", $dbUser);
        return $db[2] . "-" . $db[0] . "-" . $db[1];
    }

    /**
     * добавляем запись в базу данных
     * @return boolean
     */
    function fnInsUser($arr) {
        try {
            $query = $this->conn->prepare("INSERT INTO tabUsers (fnameUser,lnameUser,emailUser,passUser,dbUser,countryUser,fotoUser,descUser) VALUES (?,?,?,?,?,?,?,?)");
            $query->execute(array($arr['fnameUser'], $arr['lnameUser'], $arr['emailUser'], md5($arr['passUser1']), $this->fnFormatDate($arr['dbUser']), $arr['countryUser'], $arr['fotoUser'], $arr['descUser']));
            return true;
        } catch (PDOException $e) {
            echo 'Error : ' . $e->getMessage();
            exit();
        }
    }

}
